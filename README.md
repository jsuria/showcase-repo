DISCLAIMER: Temporary public repo for showcasing sample PHP/Symfony code. All rights belong to their respective owners. Variable and class names have been changed for confidentiality reasons. 

1. Contains sample bits of code abstracted from real-world applications.
2. Repo will be switched to private access once review is done.

UPDATE: Sample generic Javascript classes (Backbone JS) added. All rights belong to their respective owners. Variable and class names have been changed for confidentiality reasons. 