<?php
/**
 * Custom filter for displaying data in list format
 */
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('table_list', [$this, 'renderTable']),
        ];
    }

    public function renderTable(array $source)
    {
        $source = $params['source_variable'];
        $default_value = $params['default_value'];
        $id = $params['id'];
        $name = $params['name'];


        if(!isset($source['error_message'])){
            // Flag whether to show as a list or not
            $show_as_list = $params['show_as_list'];

            if ($show_as_list) {
                $html = '<select name="' . $name . '" id="' . $id . '" style="margin-left: 0px !important;">';

                foreach ($source as $phone_type) {
                    $description =  str_replace('Type:', '', $phone_type['description']);
                    $clean_description = $description == 'text' ? 'Text (SMS)' : 'Voice';

                    $value =  $phone_type['value'];

                    $html .= '<option value="' . $value . '" ' . ($default_value == $value ? 'selected' : '') . '>'
                          . $clean_description
                          . '</option>';
                }

                $html .= '</select>';
            } else {
                // For display only (show the current value)
                $description =  str_replace('Type:', '', $default_value);
                $clean_description = $description == 'text' ? 'Text (SMS)' : 'Voice';

                $html = '<input type="text" value="'
                        . $clean_description
                        . '" style="margin:0px; width: 65%" disabled/>';
            }
        } else {
            $html = $source['error_message'];
        }

        return $html;
    }
}