/**
 * Storage Cache
 * JavaScript
 * @author    Jose Suria
 */
define([], function() {
	 /** 
     * Constructor 
     * TODO: In the future, find a better implementation of storing objects
     */
	var Storage = function Storage(){
		this.cache = {};	// Main property used for iteration and getting keys
		this.array = [];	// A numerically-indexed copy of the storage
	};

    Storage.prototype = {

    	/**
         * Add an item
         * 
         * @param {string}   key
         * @param {variant}  val
		 * 
         * @return void
         */
		setItem : function(key, val)
	    {
	    	// If with duplicate, remove old value	    	
	    	if(this.hasKey(key)){
				this.removeItem(key);				
	    	}

	    	// Fill both object and array
	    	this.cache[key] = val;
	    	this.array.push({
	    		Key : key,
	    		Value : val
	    	});
	    },

	    /**
         * Remove the item from both array and object using key
         * 
         * @param  {string}  key
		 * 
         * @return void
         */
	    removeItem : function(key)
	    {
	    	var self = this;

	    	delete self.cache[key];

	    	// Get index of element to be removed from array
	    	var i = self.array.findIndex(function(item){
	    		//return item.Key != undefined;
	    		return item.Key === key;
	    	});

	    	if(i > -1){
	    		self.array.splice(i,1);	
	    	}
	    },

	    /**
         * Get an item
         * 
         * @param  {string}  key
		 * 
         * @return variant
         */
	    getItem : function(key)
	    {
	    	return this.cache[key];
	    },
	    
	    /**
         * Checks the cache for a key
         * 
         * @param  {string} key
		 * 
         * @return boolean
         */
	    hasKey: function(key)
	    {
	    	return this.cache[key] !== undefined;
	    },

	    /**
         * An alias; returns the cache as an object
         * 
         * @return object
         */
	    asObject: function()
	    {
	    	return this.cache;
	    },

	    /**
         * Returns the cache as an array
         * Use sparingly! Not 100% compatible with nested objects
		 * 
         * @param  {string} key
		 * 
         * @return boolean
         */	    
	    asArray: function()
	    {
			return this.array;
	    },

	    /**
         * Returns the length of the cache
         * 
         * @return int
         */
	    length: function()
	    {
	    	return this.array.length;
	    },

	    /**
         * Resets the cache (both object and array)
         * 
         * @return object
         */
	    clear: function()
	    {
	    	this.cache = {};
	    	this.array = [];
	    }
	};

    return Storage;
});