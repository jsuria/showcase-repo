<?php

/**
 * AnotherController
 *
 * PHP version 5
 * @author Jose Suria

 */
//...
namespace BigApp\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AnotherController extends Controller
{
    // Private variables here...

    /**
     * Gets all Info
     * @Route("/getacronyminfo/{cId}/{uId}")
     * @Method({"GET"})
     */
    public function getAcronymInfo($cId, $uId)
    {
        try {
            // Data source is a .NET/C# webservice which encapsulates the DB calls
            $modhandler = new \BigApp\CommercialMod\SomeProduct\SomeTechieSoundingAcronym(
                $this->_someWebService,
                $cId,
                $uId
            );

            $emailInfo = $modhandler->getEmailInfo();
            $acronymInfo['email'] = $emailInfo['data'];

            $acronymInfo['phones']['1'] = $this->getmodhandlerPhone('1', $cId, $uId);
            $acronymInfo['phones']['2'] = $this->getmodhandlerPhone('2', $cId, $uId);
            
            return $this->render('subproduct/acronym-info.html.twig', [
                'ACR_INFO' =>  $acronymInfo
            ]);

        } catch (\Exception $e) {
            $acronymInfo['message'] = $e->getMessage();
            return $this->render('utilities/exception.html.twig', [
                'ACR_INFO' => $acronymInfo
            ]);
        }
    }

    /**
     * DisplayUpdate Form
     * @Route("/showupdateform/{cId}/{uId}/{flag}")
     * @Method({"GET"})
     */
    public function showUpdateForm($cId, $uId, $flag)
    {
        $editFlag  = $flag;

        // Data source is a .NET/C# webservice which encapsulates the DB calls
        $modhandler = new \BigApp\CommercialMod\SomeProduct\SomeTechieSoundingAcronym(
            $this->_someWebService,
            $cId,
            $uId
        );

        $mode = ['email','phone1', 'phone2'];

        $renderTypes = $modhandler->getTypeOptions();
        $renderMode = $editFlag;
        
        switch ($mode[$editFlag]) {
            case 'email':
                $emailInfo = $modhandler->getEmailInfo();
                $renderInfo = $emailInfo['data']['value'];
                break;
            case 'phone1':
                $phoneInfo['1'] = $this->getAcrPhone('1', $cId, $uId);
                $renderInfo = $phoneInfo;
                break;
            case 'phone2':
                $phoneInfo['2'] = $this->getAcrPhone('2', $cId, $uId);
                $renderInfo = $phoneInfo;
                break;
            default:
                // Any default behaviour, if ever...
        }

        return $this->render('forms/update-form.html.twig', [
            'C_ID' => $cId,
            'U_ID' => $uId,
            'ACRONYM_FORM_PHONE_TYPES' => $renderTypes,
            'ACRONYM_FORM_EDIT_MODE' => $renderMode,   
            'ACRONYM_FORM_INFO' => $renderInfo
        ]);
    }
}

?>