<?php
/**
 * SomeTechieSoundingAcronym Model
 *
 * PHP version 5
 * @author Jose Suria

 */
namespace BigApp\CommercialMod\SomeProduct;

class SomeTechieSoundingAcronym extends SomeGenericCommercialClass
{
    private $_c_id;
    private $_u_id;
    private $_category;
    private $_sub_categories;
    private $_phone_attributes;
    private $_phone_types;
    private $_email_attributes;
    private $_cvd_ws;
    private $_cvd_tbl_name;
    private $_cvd_col_name;

    /**
     * Initializations, implicitly call parent since we're overriding main constructor
     */
    public function __construct(\BigApp\WebServiceType $ws, $c_id, $u_id)
    {
        parent::__construct($ws);

        $this->_c_id = $c_id;
        $this->_u_id = $u_id;
        $this->_category = "**********";
        $this->_sub_categories = array(
                'email'  => '*****************',
                'phone1' => '*****************',
                'phone2' => '*****************'
            );

        /****************************************/
        /* Modify as needed, more may be added */
        /****************************************/

        $this->_phone_types = array(
                'Type:voice'  => '*****************',
                'Type:text'   => '*****************'
            );
        // Type of data, used for render_type parameter
        $this->_phone_attributes = array(
                'value' => '***',
                'type' => '***',
            );
        // As assigned by WS
        $this->_email_attributes = array(
                'default' => '*****'
            );

        // CVD parameters
        $this->_cvd_ws = \BigApp\WebServiceType::getInstance('ws_entity');

        $this->_cvd_tbl_name = '*********';
        $this->_cvd_col_name = '*********';
    }

    /**
     * Gets phone types
     * @return array
     *
     */
    public function getTypeOptions()
    {
        $result = $this->_cvd_ws->GetColumnValueDescription(
            array(
                'table_name' => $this->_cvd_tbl_name,
                'column_name' => $this->_cvd_col_name
            )
        );

        return $result;
    }
    
    /**
     * Returns the OTP email
     * @return array
     */
    public function getEmailInfo()
    {
        $result =  $this->getWS()->GetSomeTagByCategorySubCategory(
            array(
                'c_id' => $this->_c_id,
                'u_id' => $this->_u_id,
                'category' => $this->_category,
                'sub_category' => $this->_sub_categories['email'],
                'render_type' => $this->_email_attributes['default']
            )
        );

        return $this->getResult($result);
    }

    /**
     * Returns the specified phone info (label and attribute)
     * @return array
     */
    public function getPhoneInfo($params)
    {
        $postFix = $params['phone_postfix'];
        $attribute = $params['phone_attribute'];

        $result =  $this->getPhoneAttribute(
            array(
                'phone' => $this->_sub_categories['phone' . $postFix],
                'attribute' => $this->_phone_attributes[$attribute]
            )
        );

        return $this->getResult($result);
    }

    /**
     * Updates OTP email
     * @return void
     */
    public function setEmail($params)
    {
        $validation = new Validation();
        $validation->addValidator('acr_email', $validation->emailaddress('Email Address'));
        $validation->assert('<br>', null, $params);
        $request = $validation->getInput();

        // Precaution in case $request->acr_email
        // (happens when $validation->getInput()->getEscaped() is used)
        if (!empty($request->acr_email)) {
            $result = $this->getWS()->SetSomeTagByCategorySubCategory(
                array(
                    'c_id' => $this->_c_id,
                    'u_id' => $this->_u_id,
                    'category' => $this->_category,
                    'sub_category' => $this->_sub_categories['email'],
                    'render_type' => $this->_email_attributes['default'],
                    'value' => $request->acr_email
                )
            );
        } else {
            $result['message'] = "Notice: Email value not provided.";
        }

        return $this->getResult($result);
    }

    /**
     * Updates OTP phone
     * @return void
     */
    public function setPhone($params)
    {
        $validation = new Validation();

        $validation->addValidator('acr_phone1_value', $validation->phonenumber('ACR Phone 1'));
        $validation->addValidator('acr_phone1_type', $validation->inArray(
            'ACR Phone 1 Type',
            array_keys(
                $this->_phone_types
            )
        ));
        $validation->addValidator('acr_phone2_value', $validation->phonenumber('ACR Phone 2'));
        $validation->addValidator('acr_phone2_type', $validation->inArray(
            'ACR Phone 2 Type',
            array_keys(
                $this->_phone_types
            )
        ));
        $validation->assert('<br>', null, $params);
        $request = $validation->getInput();

        // Phone 1
        /************************************************************************/
        // Re-adding the checking for empty values
        if (!empty($request->acr_phone1_value)) {
            $results['phone1']['value'] = $this->setPhoneAttribute(
                array(
                    'phone' => $this->_sub_categories['phone1'] ,     // Phone 1
                    'attribute' => $this->_phone_attributes['value'], // Value attribute
                    'attribute_value' => $request->acr_phone1_value   // Value
                )
            );
            
            // Stack messages if encountered
            if (!empty($results['phone1']['value']['message'])) {
                $results['messages'][] = $results['phone1']['value']['message'];
            }

            $results['phone1']['type'] = $this->setPhoneAttribute(
                array(
                    'phone' => $this->_sub_categories['phone1'],      // Phone 1
                    'attribute' => $this->_phone_attributes['type'],  // Type attribute
                    'attribute_value' => $request->acr_phone1_type    // Value
                )
            );
            if (!empty($results['phone1']['type']['message'])) {
                $results['messages'][] = $results['phone1']['type']['message'];
            }
        }

        // Phone 2
        /************************************************************************/
        // Re-adding the checking for empty values
        if (!empty($request->acr_phone2_value)) {
            $results['phone2']['value'] = $this->setPhoneAttribute(
                array(
                    'phone' => $this->_sub_categories['phone2'],          // Phone 1
                    'attribute' => $this->_phone_attributes['value'],     // Value attribute
                    'attribute_value' => $request->acr_phone2_value       // Value
                )
            );
            if (!empty($results['phone2']['value']['message'])) {
                $results['messages'][] = $results['phone2']['value']['message'];
            }

            $results['phone2']['type'] = $this->setPhoneAttribute(
                array(
                    'phone' => $this->_sub_categories['phone2'],          // Phone 1
                    'attribute' => $this->_phone_attributes['type'],      // Value attribute
                    'attribute_value' => $request->acr_phone2_type        // Value
                )
            );
            if (!empty($results['phone2']['type']['message'])) {
                $results['messages'][] = $results['phone2']['type']['message'];
            }
        }

        return $results;
    }

    /**
     * Get ACR Phone attribute (type or number)
     * @return array $params Params
     * @return array
     */
    private function getPhoneAttribute($params)
    {
        return $this->getWS()->GetAccurintTagByCategorySubCategory(
            array(
                'c_id' => $this->_c_id,
                'u_id' => $this->_u_id,
                'category' => $this->_category,
                'sub_category' => $params['phone'],
                'render_type' => $params['attribute']
            )
        );
    }

    /**
     * Updates ACR Phone attribute (type or number)
     * @return array $params Params
     * @return array
     */
    private function setPhoneAttribute($params)
    {
        $result =  $this->getWS()->SetAccurintTagByCategorySubCategory(
            array(
                'c_id' => $this->_c_id,
                'u_id' => $this->_u_id,
                'category' => $this->_category,
                'sub_category' => $params['phone'],
                'render_type' => $params['attribute'],
                'value' => $params['attribute_value']
            )
        );

        $returnable = $this->getResult($result);
    }

    /**
     * Processes phone types to use custom names
     * Added as fail-safe in case WS values revert
     * @return array
     *
     */
    private function useCustomPhoneTypes($payloadDuJour, $useValueAsDescription)
    {
        for ($i=0; $i < sizeof($payloadDuJour); $i++) {
            $new_value = $this->_phone_types[$payloadDuJour[$i]['value']];
            $payloadDuJour[$i]['value'] = $new_value;

            if ($useValueAsDescription) {
                $payloadDuJour[$i]['description'] = $new_value;
            }
        }

        return $payloadDuJour;
    }
}
